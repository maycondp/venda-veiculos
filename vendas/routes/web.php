<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Routes Cars
Route::group(["prefix" => "carros"], function (){

    Route::get("/", "CarrosController@index");
    Route::get("/usuario", "CarrosController@indexUsuario");
    Route::get("/login", "CarrosController@login");
    Route::get("/novo", "CarrosController@novoView");
    Route::get("/{id}/editar", "CarrosController@editarView");
    Route::get("/{id}/excluir", "CarrosController@excluirView");
    Route::get("/{id}/destroy", "CarrosController@destroy");
    Route::post("/store", "CarrosController@store");
    Route::post("/update", "CarrosController@update");
    Route::post("/buscaAno", "CarrosController@buscaAno");

});

//Routes Peoples
Route::group(["prefix" => "usuarios"], function () {

    Route::get("/usuario", "UsuariosController@novoView");
    Route::get("/login", "UsuariosController@indexLogin");
    Route::post("/store", "UsuariosController@store");


});
