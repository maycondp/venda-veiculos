@extends('template.app')

@section('content')

    <div class="col-md-6 well">
        <div class="col-md-12">
            <h3>Deseja excluir este veiculo?</h3>
            <div style="float: right">

                <a class="btn btn-default" href="{{url("carros")}}">
                    <i class="glyphicon glyphicon-chevron-left"></i>
                    &nbsp;Cancelar
                </a>

                <a class="btn btn-danger" href="{{url("/carros/$carro->id/destroy")}}">
                    <i class="glyphicon glyphicon-remove"></i>
                    &nbsp;Excluir
                </a>
            </div>
        </div>
    </div>

    <div class="col-md-3">
        <div class="panel panel-danger">
            <div class="panel-heading">{{ $carro->modelo }}</div>
            <div class="panel-body">
                <p><strong>Marca: </strong>{{ $carro->marca }}</p>
                <p><strong>Cor: </strong>{{ $carro->cor }}</p>
                <p><strong>Ano: </strong>{{ $carro->ano }}</p>
                <p><strong>Preco: </strong>{{ $carro->preco }}</p>

            </div>
        </div>
    </div>

@endsection