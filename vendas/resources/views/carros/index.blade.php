@extends("template.app")

@section("content")

    <style>
        .btn{
            padding: 5px;
            margin-left: 5px;
            float: right;
        }
    </style>

    <div>
        <div class="col-lg-10">
                <form action="{{url('/carros/buscaAno')}}" method="post">
                    <div style="margin-top: 5px" class="col-sm-4 input-group">
                        {{ csrf_field() }}
                        <input type="text" class="form-control" name="criterio" placeholder="Ano">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="submit">Ok</button>
                        </span>
                    </div>
                </form>
        </div>


        @foreach($carros as $carro)

            <div class="col-md-3">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        {{ $carro->modelo }}

                        <a href="{{url("/carros/$carro->id/excluir")}}" class="btn btn-xs btn-danger btn-action">
                            <i class="glyphicon glyphicon-trash"></i>
                        </a>

                        <a href="{{url("/carros/$carro->id/editar")}}" class="btn btn-xs btn-primary btn-action">
                            <i class="glyphicon glyphicon-pencil"></i>
                        </a>
                    </div>
                    <div class="panel-body">
                        <p><strong>ID: </strong>{{ $carro->id }} </p>
                        <p><strong>Marca: </strong>{{ $carro->marca }} </p>
                        <p><strong>Cor: </strong>{{ $carro->cor }} </p>
                        <p><strong>Ano: </strong>{{ $carro->ano }} </p>
                        <p><strong>Preco: </strong>{{ $carro->preco }} </p>

                    </div>

                </div>
            </div>
        @endforeach
    </div>
@endsection