@extends('template.app')

@section('content')

    <div class="col-md-12">
        <h3>Editar Veiculo</h3>
    </div>

    <div class="col-md-6 well">
        <form class="col-md-12" action="{{url('/carros/update')}}" method="post">
            {{csrf_field()}}
            <input type="hidden" name="id" value="{{ $carro->id }}">
            <div class="from-group">
                <label class="control-label"></label>
                <input name="modelo" value="{{ $carro->modelo }}" class="col-md-12 form-control" placeholder="Modelo">
                <input style="margin-top: 25px" name="marca" value="{{ $carro->marca }}" class="col-md-12 form-control" placeholder="Marca">
                <input style="margin-top: 25px" name="cor" value="{{ $carro->cor }}" class="col-md-12 form-control" placeholder="Cor">
                <input style="margin-top: 25px" name="ano" value="{{ $carro->ano }}" class="col-md-12 form-control" placeholder="Ano">
                <input style="margin-top: 25px" name="preco" value="{{ $carro->preco }}" class="col-md-12 form-control" placeholder="Preco">

            </div>

            <div class="col-md-12">
                <button style="margin-top: 15px" class="btn-primary">Salvar</button>
            </div>
        </form>
    </div>

@endsection
