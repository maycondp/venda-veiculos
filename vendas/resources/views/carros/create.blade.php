@extends('template.app')

@section('content')

    <div class="col-md-12">
        <h3>Novo Carro</h3>
    </div>

    <div class="col-md-6 well">
        <form class="col-md-12" action="{{url('/carros/store')}}" method="post">
            {{csrf_field()}}
            <div class="from-group">
                <label class="control-label"></label>
                <input name="modelo" class="col-md-12 form-control" placeholder="Modelo">
                <input style="margin-top: 25px" name="marca" class="col-md-12 form-control" placeholder="Marca">
                <input style="margin-top: 25px" name="cor" class="col-md-12 form-control" placeholder="Cor">
                <input style="margin-top: 25px" name="ano" class="col-md-12 form-control" placeholder="Ano">
                <input style="margin-top: 25px" name="preco" class="col-md-12 form-control" placeholder="Preco">



            </div>
            <button style="margin-top: 15px" class="btn-primary">Salvar</button>
        </form>
    </div>
@endsection