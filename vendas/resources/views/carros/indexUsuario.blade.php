@extends("template.app2")

@section("content")

    <style>
        .btn{
            padding: 5px;
            margin-left: 5px;
            float: right;
        }
    </style>

    <div>
        @foreach($carros as $carro)

            <div class="col-md-3">
                <div class="panel panel-info">
                    <div class="panel-heading"><center>{{ $carro->modelo }}</center></div>

                    <div class="panel-body">
                        <p><strong>Marca: </strong>{{ $carro->marca }} </p>
                        <p><strong>Cor: </strong>{{ $carro->cor }} </p>
                        <p><strong>Ano: </strong>{{ $carro->ano }} </p>
                        <p><strong>Preco: </strong>{{ $carro->preco }} </p>

                    </div>

                </div>
            </div>
        @endforeach
    </div>
@endsection