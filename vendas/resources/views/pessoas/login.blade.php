@extends("template.app2")

@section("content")

    <div class="col-md-12">
        <h3>Login</h3>
    </div>

    <div class="col-md-6 well">
        <form class="col-md-12" action="{{url('')}}" method="post">
            {{csrf_field()}}
            <div class="from-group">
                <label class="control-label"></label>
                <input style="margin-top: 25px" name="usuario" class="col-md-12 form-control" placeholder="Usuario">
                <input style="margin-top: 25px" name="senha" class="col-md-12 form-control" placeholder="Senha">
            </div>
            <button style="margin-top: 15px" class="btn-primary">Cadastrar</button>
        </form>
    </div>
@endsection