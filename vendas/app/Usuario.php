<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Usuario extends Model
{
    protected $fillable = [
        'id',
        'nome',
        'cpf',
        'usuario',
        'senha'

    ];

    protected $table = 'usuarios';
}
