<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Carro extends Model
{

    protected $fillable = [
        'id',
        'modelo',
        'marca',
        'cor',
        'ano',
        'preco'
    ];


    protected $table = 'carros';
}
