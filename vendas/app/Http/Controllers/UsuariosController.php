<?php

namespace App\Http\Controllers;

use App\Usuario;
use Illuminate\Http\Request;

class UsuariosController extends Controller
{
    public function indexLogin()
    {
        return view('pessoas.login');
    }

    public function novoView()
    {
        return view('pessoas.create');
    }

    public function store(Request $request)
    {
        $usuario = new Usuario();
        $usuario->id = null;
        $usuario->nome = $request->get('nome');
        $usuario->cpf = $request->get('cpf');
        $usuario->usuario = $request->get('usuario');
        $usuario->senha = $request->get('senha');
        $usuario->save();

        return redirect("/usuarios/login")->with("message", "Login realizado com sucesso!");
    }
}
