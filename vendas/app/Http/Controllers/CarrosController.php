<?php

namespace App\Http\Controllers;

use App\Carro;
use Illuminate\Http\Request;

class CarrosController extends Controller
{

    private $carro;

    /**
     * CarrosController constructor.
     * @param $carro
     */
    public function __construct()
    {
        $this->carro = new Carro();
    }

    public function index()
    {
        $list_carros = Carro::all();

        return view('carros.index', [
            'carros' => $list_carros
        ]);
    }

    public function indexUsuario()
    {
        $list_carros = Carro::all();

        return view('carros.indexUsuario', [
            'carros' => $list_carros

        ]);
    }

    public function buscaAno(Request $request)
    {
        $carros = Carro::where('ano', 'LIKE', '%' . $request->criterio . '%')->get();

        return view('carros.index', [
            'carros' => $carros,
            'criterio' => $request->criterio

        ]);
    }

    public function novoView()
    {
        return view('carros.create');
    }

    public function store(Request $request)
    {
       $carro = new Carro();
       $carro->id = null;
       $carro->modelo = $request->get('modelo');
       $carro->marca = $request->get('marca');
       $carro->cor = $request->get('cor');
       $carro->ano = $request->get('ano');
       $carro->preco = $request->get('preco');
       $carro->save();
        
        return redirect("/carros")->with("message", "Carro cadastrado com sucesso!");

    }

    public function excluirView($id)
    {
        return view('carros.delete', [
            'carro' => $this->getCarro($id)
        ]);
    }

    public function destroy($id)
    {
        $this->getCarro($id)->delete();

        return redirect(url('carros'))->with('success', 'Excluido');
    }

    public function editarView($id)
    {
        return view('carros.edit', [
            'carro' => $this->getCarro($id)
        ]);
    }

    public function update(Request $request)
    {
        $carro = $this->getCarro($request->id);
        $carro->update($request->all());

        return redirect('/carros');
    }

    protected function getCarro($id)
    {
        return $this->carro->find($id);
    }

}
